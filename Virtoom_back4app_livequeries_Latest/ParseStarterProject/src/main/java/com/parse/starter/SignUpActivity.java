package com.parse.starter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

// Code adapted from
// https://developer.android.com/guide/topics/ui/controls/spinner

public class SignUpActivity extends AppCompatActivity implements View.OnKeyListener, View.OnClickListener {
    Spinner facultySpinner;
    Spinner institutionSpinner;

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        // When enter is pressed the sign up code is initiated
        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

            signUp(view);

        }

        return false;
    }

    @Override
    public void onClick(View view) {
        // when the background or the logo are clicked the keyboard is removed, removing any obstructions from the user
        if (view.getId() == R.id.background || view.getId() == R.id.logo) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

    }

    public void signUp(View view) {

        //Creating an alert dialog before entering the user's details in the database accept the terms and conditions
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Terms and Conditions")
                .setMessage(Html.fromHtml("Do you agree with the following <a href=\"https://www.google.com\">Terms and Conditions?</a>"))
                .setPositiveButton("Yes, I agree", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // If the user agrees with the terms and conditions

                        //Obtaining the name, email and password text field
                        EditText nameText = (EditText) findViewById(R.id.username);
                        EditText emailText = (EditText) findViewById(R.id.email);
                        EditText passwordText = (EditText) findViewById(R.id.password);


                        // Obtaining the text in the text fields above and the chosen option from the drop down menu
                        String nameSurname = nameText.getText().toString();
                        String email = emailText.getText().toString();
                        String password = passwordText.getText().toString();
                        String institution = institutionSpinner.getSelectedItem().toString();
                        String faculty = facultySpinner.getSelectedItem().toString();

                        // Making sure that all the fields were entered
                        if (nameSurname.matches("") || email.matches("") || password.matches("") || institution.matches("Please choose institution") || faculty.matches("Please choose faculty")){
                            Toast.makeText(SignUpActivity.this,"Enter all fields!", Toast.LENGTH_LONG).show();
                        } else {
                            // Creating a new user instance
                            ParseUser user = new ParseUser();

                            // Initializing the user details to the user instance
                            user.setUsername(nameSurname);
                            user.setEmail(email);
                            user.setPassword(password);
                            user.put("institution", institution);
                            user.put("faculty", faculty);
                            user.put("points", 0);

                            // Signing up the user and sending the user to the main screen
                            user.signUpInBackground(new SignUpCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {

                                        Toast.makeText(SignUpActivity.this, "Sign Up Successful", Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
                                        startActivity(intent);

                                    } else {

                                        Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                        }

                    }
                })
                .setNegativeButton("I do not agree",null)
                .show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setTitle("Virtoom");

        // Obtaning the logo and background view
        ImageView logo = (ImageView) findViewById(R.id.logo);
        ConstraintLayout background = (ConstraintLayout) findViewById(R.id.background);

        // Setting institution spinner
        institutionSpinner = (Spinner) findViewById(R.id.institutionSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.institutions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        institutionSpinner.setAdapter(adapter1);

        // Setting facility spinner
        facultySpinner = (Spinner) findViewById(R.id.facultySpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.faculties, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        facultySpinner.setAdapter(adapter);


        // Setting listeners on the background on logo, so when these are pressed the Keyboard will drop down
        background.setOnClickListener(this);
        logo.setOnClickListener(this);

    }
}
