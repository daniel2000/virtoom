package com.parse.starter;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.livequery.ParseLiveQueryClient;
import com.parse.livequery.SubscriptionHandling;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoomChat extends AppCompatActivity {

    String roomName = "";
    String roomTopic = "";
    String currentUser = "";
    String admin = "";

    List<Map<String,String>> messageData;
    SimpleAdapter simpleAdapter = null;

    ListView chatListView = null;

    int userPoints;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.chat_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The menu has 3 options
        // Creating a quiz, attempting a quiz and view quiz results
        if(item.getItemId() == R.id.createQuiz) {
            // When the user wants to create a quiz
            Intent intent = new Intent(getApplicationContext(), CreateQuiz.class);
            intent.putExtra("roomName", roomName);
            startActivity(intent);
        } else if(item.getItemId() == R.id.attemptQuiz) {
            // When the user wants to attempt a quiz
            Intent intent = new Intent(getApplicationContext(), AnswerQuiz.class);
            intent.putExtra("roomName", roomName);
            startActivity(intent);

        } else if(item.getItemId() == R.id.viewQuiz) {
            // For the user to view his quiz
            Intent intent = new Intent(getApplicationContext(), ViewQuizResults.class);
            intent.putExtra("roomName", roomName);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_chat);

        setupRoomChat();
        setUpLiveQueryHandler();
        chatListView.smoothScrollToPosition(messageData.size()-1);
    }

    private void setupRoomChat(){
        Intent intent = getIntent();

        messageData = new ArrayList<>();

        currentUser = ParseUser.getCurrentUser().getUsername();
        //Getting the extra values passed from the intent to avoid redundant server requests for basic room information
        roomName = intent.getStringExtra("roomName");
        roomTopic = intent.getStringExtra("topic");
        admin = intent.getStringExtra("admin");

        getSupportActionBar().setTitle("Room Title: " + roomName);
        getSupportActionBar().setSubtitle("Room owner: " + admin);

        chatListView = (ListView) findViewById(R.id.roomChatList);

        //Similarly to that of chat fragment where we assign appropriate colours to make it more readable
        simpleAdapter = new SimpleAdapter(RoomChat.this, messageData, android.R.layout.simple_list_item_2, new String[] {"content", "date"}, new int[] {android.R.id.text1, android.R.id.text2}){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);

                TextView textView = (TextView) view.findViewById(android.R.id.text1);

                SpannableString ss = new SpannableString(textView.getText().toString());

                ForegroundColorSpan appliedColor;

                if(textView.getText().toString().split(":")[0].compareTo(currentUser) == 0){
                    appliedColor = new ForegroundColorSpan(Color.BLUE);
                }else{
                    appliedColor = new ForegroundColorSpan(Color.DKGRAY);
                }

                ss.setSpan(appliedColor, textView.getText().toString().indexOf(":")+1, textView.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                textView.setText(ss);

                return view;
            }
        };

        chatListView.setAdapter(simpleAdapter);

        ParseQuery<ParseObject> query = new ParseQuery<>("RoomChat");
        query.whereEqualTo("title", roomName);

        query.orderByAscending("createdAt");

        //Loading all messages of the room with this title
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        messageData.clear();
                        for(ParseObject message : objects){
                            Map<String,String> messageInfo = new HashMap<>();

                            messageInfo.put("content", message.getString("message"));

                            messageInfo.put("date", message.getString("actualDate"));

                            messageData.add(messageInfo);
                        }
                        simpleAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void setUpLiveQueryHandler(){
        ParseLiveQueryClient parseLiveQueryClient = null;

        try{
            parseLiveQueryClient = ParseLiveQueryClient.Factory.getClient(new URI("wss://virtoom.back4app.io/"));
        }catch(URISyntaxException e){
            e.printStackTrace();
        }

        if(parseLiveQueryClient != null){
            //Setting up the live query for real time chatting
            Log.i("Client","Client live query working");

            ParseQuery<ParseObject> parseQuery = new ParseQuery<>("RoomChat");
            parseQuery.whereEqualTo("title", roomName);


            SubscriptionHandling<ParseObject> subscriptionHandling = parseLiveQueryClient.subscribe(parseQuery);

            subscriptionHandling.handleEvent(SubscriptionHandling.Event.CREATE, new SubscriptionHandling.HandleEventCallback<ParseObject>() {
                @Override
                public void onEvent(ParseQuery<ParseObject> query, final ParseObject object) {
                    //If a create event happens then it means that there is a new message in the room therefore we update the list and include the new message
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //Log.i("Success!", "You received a new message");
                            Map<String,String> messageInfo = new HashMap<>();

                            messageInfo.put("content", object.getString("message"));

                            messageInfo.put("date", object.getString("actualDate"));

                            messageData.add(messageInfo);
                            simpleAdapter.notifyDataSetChanged();
                            chatListView.smoothScrollToPosition(messageData.size()-1);
                        }
                    });
                }
            });

        }
    }

    public void sendRoomChat(View view) throws ParseException {
        final EditText chatEditText = (EditText) findViewById(R.id.roomChatEditText);

        final String messageContent = chatEditText.getText().toString();

        final Calendar cal = Calendar.getInstance();

        final SimpleDateFormat dateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        // Adding to the user's Virtoom Points
        ParseQuery<ParseUser> query = ParseUser.getQuery();

        // Getting the current user
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());

        // Finding the query instantly and therefore finding the current points the user has
        List<ParseUser> user = query.find();
        user.get(0).get("points");
        String currentPoints = user.get(0).get("points").toString();
        int points = Integer.parseInt(currentPoints)+1;

        // Adding and saving the new points the user has
        user.get(0).put("points", points);
        userPoints = points;
        user.get(0).saveInBackground();


        ParseObject message = new ParseObject("RoomChat");


        message.put("title", roomName);
        message.put("sender", currentUser);
        message.put("message", currentUser + " (" +  userPoints  + ")" + ": " + messageContent);
        message.put("actualDate", dateTime.format(cal.getTime()));

        chatEditText.setText("");
        //Saving the message sent in the room
        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    //finished
                }
            }
        });

    }
}
