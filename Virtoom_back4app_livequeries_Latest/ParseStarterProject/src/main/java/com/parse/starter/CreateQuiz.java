package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.R;

public class CreateQuiz extends AppCompatActivity {

    String roomName = "";


    public void generateQuiz(View view){
        // Obtaining the quiz title inputted by the user
        EditText title = (EditText) findViewById(R.id.quizTitle);
        String titleText = "";
        titleText = title.getText().toString();

        // Making sure the title is not empty, else warning the user
        if(titleText.matches("")){
            Toast.makeText(CreateQuiz.this,"Enter title!", Toast.LENGTH_LONG).show();
        } else {
            // Creating a new entry in the Quiz Table
            final ParseObject quiz = new ParseObject("Quiz");

            // Adding the the title of the Quiz, the user who created the user and on which room it was created
            quiz.put("title",titleText);
            quiz.put("creatorID", ParseUser.getCurrentUser());
            quiz.put("roomName", roomName);

            // Save the quiz in the background
            quiz.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e==null){
                        // Redirect user to the enter questions
                        Intent intent = new Intent(getApplicationContext(), AddQuestionToQuiz.class);
                        intent.putExtra("quizId", quiz.getObjectId());

                        startActivity(intent);
                    }else{
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);

        // Obtaining the room name from the calling intent
        roomName = getIntent().getStringExtra("roomName");

        setTitle("Virtoom");
    }
}
