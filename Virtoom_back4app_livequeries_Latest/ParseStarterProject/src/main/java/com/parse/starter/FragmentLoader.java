package com.parse.starter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.parse.ParseUser;
import com.parse.starter.MainActivity;
import com.parse.starter.R;

//Adapted the code from https://www.youtube.com/watch?v=tPV8xA7m-iw

public class FragmentLoader extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.user_list_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.logout) {
            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_loader);

        loadNavBar();
    }

    private void loadNavBar(){
        setTitle("Users");

        BottomNavigationView bottomNav = (BottomNavigationView) findViewById(R.id.bottom_navbar);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFrag = null;

                //Checking which button was pressed on the navigation bar to change the fragment
                switch (item.getItemId()){
                    case R.id.nav_home:
                        setTitle("Users");
                        selectedFrag = new HomeFragment();
                        break;

                    case R.id.nav_profile:
                        setTitle("Your Profile");
                        selectedFrag = new UserProfileFragment();
                        break;

                    case R.id.nav_room:
                        setTitle("Rooms");
                        selectedFrag = new RoomListFragment();
                        break;
                }

                if(selectedFrag != null){
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_container, selectedFrag).commit();
                }

                return true;
            }
        });

        //Initially setting the current page to the Users list which is the home fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.content_container, new HomeFragment()).commit();
    }
}
