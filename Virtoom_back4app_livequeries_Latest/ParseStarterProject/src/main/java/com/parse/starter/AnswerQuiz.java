package com.parse.starter;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

// Code adapted from
//https://www.codota.com/code/java/methods/android.widget.RadioGroup/getCheckedRadioButtonId

public class AnswerQuiz extends AppCompatActivity {

    String roomName = "";
    int currentQuestionNo = 0;
    ArrayList<ArrayList<String>> questionsList;
    int userScore = 0;
    int temp = 0;
    String quizId = "";

    final ArrayList<RadioButton> radioButtons = new ArrayList<>();


    public void LoadQuestion() {
        // Obtaining all the radio buttons and adding them to a list
        RadioButton ans1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton ans2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton ans3 = (RadioButton) findViewById(R.id.radioButton3);
        RadioButton ans4 = (RadioButton) findViewById(R.id.radioButton4);

        radioButtons.add(ans1);
        radioButtons.add(ans2);
        radioButtons.add(ans3);
        radioButtons.add(ans4);

        // Obtaining the current question, question and answers
        ArrayList<String> currentQuestionList = questionsList.get(currentQuestionNo);

        // Setting the question text from the first element of the list
        TextView questionTxt = (TextView) findViewById(R.id.questionText);
        questionTxt.setText(currentQuestionList.get(0));

        // Adding the answers options to the radio buttons
        for(int i = 2; i<currentQuestionList.size();i++) {
            radioButtons.get(i-2).setText(currentQuestionList.get(i));
        }

        // If question contained less than 4 answers, the rest of the radio buttons are filled with a '/'
        if (currentQuestionList.size() < 6) {
            for (int i = currentQuestionList.size()-1;i < 5;i++) {
                radioButtons.get(i-1).setText("/");
            }
        }
    }

    private int checkAns() {
        // Getting the current question list
        ArrayList<String> currentQuestionList = questionsList.get(currentQuestionNo);

        // Obtaining the radio button group and checking which radio button was checked
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        int userAnsId = radioGroup.getCheckedRadioButtonId();

        // Checking which radio button was checked and storing the answer number in userAnsId accordingly
        switch(userAnsId) {
            case R.id.radioButton:
                userAnsId = 1;
                break;
            case R.id.radioButton2:
                userAnsId = 2;
                break;
            case R.id.radioButton3:
                userAnsId = 3;
                break;
            case R.id.radioButton4:
                userAnsId = 4;
                break;
        }

        // Obtaining the correct answer entered by the quiz creator from the second element of the list
        int correctAns = Integer.parseInt(currentQuestionList.get(1));

        // If no radio button was selected
        if (userAnsId == -1) {
            Toast.makeText(AnswerQuiz.this,"Please choose an option!", Toast.LENGTH_SHORT).show();
            return -1;
        }

        // If the selected answer is not the correct one
        if(userAnsId != correctAns) {
            Toast.makeText(AnswerQuiz.this,"Incorrect Ans!", Toast.LENGTH_SHORT).show();
            radioGroup.clearCheck();
            return 0;

        } else {
            // When the user enters the correct answer
            Toast.makeText(AnswerQuiz.this,"Correct!", Toast.LENGTH_SHORT).show();
            radioGroup.clearCheck();
            return 1;
        }


    }


    public void LoadQuestionAndStoreAns(View view) {
        // If the user pressed start to go to the first question, the button still has the start text associated to it
        // Therefore the button text is now changed to next question and the first question is loaded
        if(temp == 0) {
            // Changing the button text
            Button startBtn = (Button) findViewById(R.id.next);
            startBtn.setText("Next Question");

            // Loading the question
            LoadQuestion();
            temp++;
        } else if(temp == 1) {
            // Checking if the correct answer was chosen
            int result = checkAns();

            // If no option was chosen, the function returns to allow the user to enter his option
            if (result == -1) {
                return;
            }

            // Storing the result
            userScore += result;
            currentQuestionNo++;

            temp++;

            // Loading the next question
            LoadQuestion();
        } else {
            // Checking if the correct answer was chosen
            int result = checkAns();

            // If no option was chosen, the function returns to allow the user to enter his option
            if (result == -1) {
                return;
            }

            // Storing the result
            userScore += result;
            currentQuestionNo++;

            // If the last question of the quiz has been responded
            // The quiz response is stored in the table QuizAnswers
            if (questionsList.size() == currentQuestionNo) {
                Toast.makeText(AnswerQuiz.this, "Quiz Completed!", Toast.LENGTH_SHORT).show();

                // Creating a new record in the table QuizAnswers
                final ParseObject quizAnswers = new ParseObject("QuizAnswers");

                // Adding the userID of the user who attempted the quiz, the room name of the quiz, the number of correct answers the user obtained and the quizId
                quizAnswers.put("userID", ParseUser.getCurrentUser());
                quizAnswers.put("roomName", roomName);
                quizAnswers.put("noOfCorrectAns", userScore);
                quizAnswers.put("quizId",quizId);

                quizAnswers.saveInBackground();

                // Displaying to the user the no of correct answers he got and redirecting him to the main page
                if (userScore > 0) {
                    Toast.makeText(AnswerQuiz.this,"Congratulations you scored " + userScore + " points!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(AnswerQuiz.this,"Unfortunately you got no correct answers :(", Toast.LENGTH_LONG).show();
                }

                finish();
                //Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
                //startActivity(intent);

            } else {
                // If there are more questions in the quiz, these are loaded
                LoadQuestion();

            }


        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_quiz);

        ConstraintLayout background = (ConstraintLayout) findViewById(R.id.background);

        // Obtaining the roomName from the calling intent
        roomName = getIntent().getStringExtra("roomName");

        setTitle("Virtoom");

        questionsList = new ArrayList<>();

        // Obtaining the last quiz created in this room
        ParseQuery<ParseObject> query = new ParseQuery<>("Quiz");
        query.whereEqualTo("roomName", roomName);
        query.orderByDescending("createdAt");

        // Processing the query
        try {
            List<ParseObject> queryResult = query.find();
            if(queryResult.size() > 0){
                // Getting the questions of the last quiz created and its id
                questionsList = (ArrayList<ArrayList<String>>) queryResult.get(0).get("questions");
                quizId = queryResult.get(0).getObjectId();

            } else{
                // If no quiz is found
                Toast.makeText(AnswerQuiz.this,"No quiz Found!", Toast.LENGTH_LONG).show();
                finish();
                //Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
                //startActivity(intent);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


}
