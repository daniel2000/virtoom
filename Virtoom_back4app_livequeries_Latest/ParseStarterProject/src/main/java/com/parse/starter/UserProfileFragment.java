package com.parse.starter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.starter.R;

import java.util.ArrayList;
import java.util.List;

public class UserProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_user_profile_fragment, container, false);

        displayUserDetails(v);
        return v;
    }

    public void displayUserDetails(View v) {
        final TextView username = (TextView) v.findViewById(R.id.user);
        final TextView email = (TextView) v.findViewById(R.id.email);
        final TextView institution = (TextView) v.findViewById(R.id.institution);
        final TextView faculty = (TextView) v.findViewById(R.id.faculty);
        final TextView points = (TextView) v.findViewById(R.id.points);


        ParseQuery<ParseUser> query = ParseUser.getQuery();

        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        //Finding the currently logged in user
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if(e == null) {
                    if (objects.size() == 1) {
                        for (ParseUser user : objects) {
                            //Only 1 user will be found therefore we retrieve all the information needed and place them in the text views
                            username.setText(user.getUsername());
                            email.setText(user.getEmail());
                            institution.setText(user.get("institution").toString());
                            faculty.setText(user.get("faculty").toString());
                            points.setText(user.get("points").toString());
                        }
                    } else {
                        Toast.makeText(getActivity(), "Error in obtaining user profile", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



    }
}
