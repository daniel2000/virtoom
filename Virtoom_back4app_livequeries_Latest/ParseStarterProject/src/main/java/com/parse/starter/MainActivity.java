package com.parse.starter;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener{

    boolean signUpMode = true;

    public void redirect(){
        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        username.setText("");
        password.setText("");

        if(ParseUser.getCurrentUser() != null){
            //Load the user
            Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
            startActivity(intent);
            finish();
        }
    }

    public void login(View view) {

        EditText usernameEditText = (EditText) findViewById(R.id.username);
        EditText passwordEditText = (EditText) findViewById(R.id.password);


        if (usernameEditText.getText().toString().matches("") || passwordEditText.getText().toString().matches("")) {

            Toast.makeText(this, "A username and password are required.", Toast.LENGTH_SHORT).show();

        } else {
            ParseUser.logInInBackground(usernameEditText.getText().toString(), passwordEditText.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {

                    if (user != null) {
                        //If the user successfully logged in then we will redirect the user to the home screen (home fragment)
                        redirect();
                    } else {

                        String message = e.getMessage();

                        if(message.toLowerCase().contains("java")){
                            message = e.getMessage().substring(e.getMessage().indexOf(" "));
                        }

                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                }
            });

        }

    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {

        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            login(view);
        }

        return false;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.changeToSignUp) {

            //Starting the sign up activity
            Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
            startActivity(intent);

        } else if (view.getId() == R.id.background || view.getId() == R.id.logo) {
            //Used to simply remove the keyboard when pressing on the background
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView changeToSignUp = (TextView) findViewById(R.id.changeToSignUp);

        changeToSignUp.setOnClickListener(this);

        setTitle("Virtoom");

        EditText password = (EditText) findViewById(R.id.password);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());
        password.setOnKeyListener(this);
        redirect();

        setupListeners();

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

    private void setupListeners(){
        RelativeLayout background = (RelativeLayout) findViewById(R.id.background);
        background.setOnClickListener(this);

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setOnClickListener(this);
    }
}