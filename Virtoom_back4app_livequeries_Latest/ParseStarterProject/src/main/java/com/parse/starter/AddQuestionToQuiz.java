package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


// Code Adapted from
// https://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java#:~:text=You%20can%20use%20Integer.,the%20NumberFormatException%20it%20can%20throw.
public class AddQuestionToQuiz extends AppCompatActivity {

    String quizId;


    private Boolean checkAndAddEntries() {
        // Obtaining all the fields entered by the user
        EditText question = (EditText) findViewById(R.id.question);
        EditText ans1 = (EditText) findViewById(R.id.ans1);
        EditText ans2 = (EditText) findViewById(R.id.ans2);
        EditText ans3 = (EditText) findViewById(R.id.ans3);
        EditText ans4 = (EditText) findViewById(R.id.ans4);
        EditText correctAns = (EditText) findViewById(R.id.correctAns);

        // Obtaining all the text in the above fields
        String questionTxt = question.getText().toString();
        String ans1Txt = ans1.getText().toString();
        String ans2Txt = ans2.getText().toString();
        String ans3Txt = ans3.getText().toString();
        String ans4Txt = ans4.getText().toString();
        String correctAnsText = correctAns.getText().toString();

        // Making sure that the correct number entered is an integer and between 1 and 4
        try {
            int temp = Integer.parseInt(correctAnsText);

            if (temp < 1 || temp > 4) {
                Toast.makeText(AddQuestionToQuiz.this,"Enter a correct answer between 1 and 4!", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch(NumberFormatException er) {
            Toast.makeText(AddQuestionToQuiz.this,"Enter integer correct answer!", Toast.LENGTH_LONG).show();
            return false;
        }

        // Making sure that at least the question and the first 2 answers are not empty
        if (questionTxt.matches("") || ans1Txt.matches("") || ans2Txt.matches("")) {
            Toast.makeText(AddQuestionToQuiz.this,"Enter question and at least 2 answers!", Toast.LENGTH_LONG).show();
            return false;
        } else {
            final ArrayList<String> questionList = new ArrayList<>();

            // List made up of the question at position 0
            // The correct answer at position 1
            // The options in the rest of the array

            questionList.add(questionTxt);
            questionList.add(correctAnsText);
            questionList.add(ans1Txt);
            questionList.add(ans2Txt);

            // Adding answer 3 and 4 if they are not empty
            if (!ans3Txt.matches("")) {
                questionList.add(ans3Txt);
            }
            if (!ans4Txt.matches("")) {
                questionList.add(ans4Txt);
            }

            // Storing the questions in the quiz record in the database as a list of lists
            ParseQuery<ParseObject> query = new ParseQuery<>("Quiz");
            query.whereEqualTo("objectId",quizId);

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if(e == null){
                        if(objects.size() == 1){
                            ArrayList<ArrayList<String>> currentQuestionList = (ArrayList<ArrayList<String>>) objects.get(0).get("questions");

                            if (currentQuestionList != null) {
                                // If this is not the first question being added, the current list of questions is appended with the last question and the result is stored
                                currentQuestionList.add(questionList);
                                objects.get(0).put("questions",currentQuestionList);
                                objects.get(0).saveInBackground();
                            } else {
                                // If this is the first question being added
                                ArrayList<ArrayList<String>> temp = new ArrayList<>();
                                temp.add(questionList);
                                objects.get(0).put("questions",temp);
                                objects.get(0).saveInBackground();
                            }
                        } else {
                            Toast.makeText(AddQuestionToQuiz.this,"Failed to add question", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });

            return true;
        }
    }

    public void addAnotherQuestion(View view) {
        // If the user wants to add another question, the current question is added a "recursive" call to this same activity is made
        if (checkAndAddEntries()) {
            Toast.makeText(AddQuestionToQuiz.this,"Question Added!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), AddQuestionToQuiz.class);
            intent.putExtra("quizId", quizId);

            startActivity(intent);
        }

    }

    public void createQuiz(View view){
        // When the user doesn't want to add more questions
        // The final question is added to the quiz and the user is redirected to the home page
        if(checkAndAddEntries()) {
            Toast.makeText(AddQuestionToQuiz.this,"Quiz Created!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question_to_quiz);

        // Get the quizId from the calling intent
        quizId = getIntent().getStringExtra("quizId");

        setTitle("Virtoom");
    }
}
