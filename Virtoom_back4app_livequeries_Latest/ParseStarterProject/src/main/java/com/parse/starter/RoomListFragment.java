package com.parse.starter;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.starter.R;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//Code for the floating action button adapted from : https://www.youtube.com/watch?v=XsdG_6i5YIk

public class RoomListFragment extends Fragment {
    ArrayList<String> rooms = new ArrayList<>();
    ArrayList<String> topics = new ArrayList<>();
    ArrayList<String> admins = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_room_list, container, false);

        //Generating the registered rooms
        generateRoomList(v);

        //Setting a listener to the floating action button to determine whether the user wants to create a new room
        FloatingActionButton floatingActionButton = (FloatingActionButton) v.findViewById(R.id.create_room);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            //If the user pressed on the create new room button then a new dialog will pop up to enter the details for a new room or either close it
            @Override
            public void onClick(View view) {
                DialogFragment dialog = CreateRoomDialog.newInstance();
                ((CreateRoomDialog)dialog).setCallback(new CreateRoomDialog.Callback() {
                    @Override
                    public void onActionClick(String name) {
                        Toast.makeText(getActivity(),name,Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }
        });

        // For search bar
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            SearchView search = (SearchView) v.findViewById(R.id.serachBar);
            search.setQueryHint("Search for room...");

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    try {
                        //When entering what to search, the search is performed
                        searchRoomTable(s,v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    try {
                        //While editing the query the search will take place each time to make it easier for the user
                        searchRoomTable(s,v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            });


        }



        return v;
    }

    public void generateRoomList(View v){
        rooms.clear();

        ListView roomsListView = (ListView) v.findViewById(R.id.roomsList);

        roomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //When a room is pressed we get the appropriate information of the room to pass it to the intent to make use of in the RoomChat
                Intent intent = new Intent(getActivity().getApplicationContext(), RoomChat.class);
                intent.putExtra("roomName", rooms.get(i));
                intent.putExtra("topic", topics.get(i));
                intent.putExtra("admin",admins.get(i));

                startActivity(intent);
            }
        });

        arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(),android.R.layout.simple_list_item_1,rooms){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(),R.color.plaintxt_border));

                return view;
            }
        };
        roomsListView.setAdapter(arrayAdapter);

        ParseQuery<ParseObject> query = new ParseQuery<>("Room");

        //Retrieve all rooms created
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        for(ParseObject room : objects){
                            //Storing the appropriate room information for when creating a new intent
                            rooms.add(room.getString("title"));
                            topics.add(room.getString("topic"));
                            admins.add(room.getString("creator"));
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void searchRoomTable(String s,final  View v) throws ParseException {

        ArrayList<String> matchedRoomsListView = new ArrayList<>();

        ListView roomListView = (ListView) v.findViewById(R.id.roomsList);


        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Room");

        query.whereContains("titleLowerCase",s.toLowerCase());

        List<ParseObject> matchedRooms = query.find();

        //Checking how many matched rooms were retrieved
        if (matchedRooms.size() == 0) {
            rooms.clear();
            rooms.add("No Rooms found!");
            arrayAdapter.notifyDataSetChanged();
        }

        else {
            rooms.clear();
            //For any matched rooms found, the adapter is updated accordingly
            for(ParseObject room : matchedRooms) {
                rooms.add(room.getString("title"));
            }
            arrayAdapter.notifyDataSetChanged();
        }
    }
}
