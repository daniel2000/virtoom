package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class ViewQuizResults extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quiz_results);

        setTitle("Virtoom");

        String roomName = "";
        String quizId = "";

        // Obtaining the roomName from the calling intent
        roomName = getIntent().getStringExtra("roomName");

        // Getting all the fields to add the text to
        final TextView noOfTries = (TextView) findViewById(R.id.noOfTries);
        final TextView passPercentage = (TextView) findViewById(R.id.passPercentage);
        final TextView resultTitle = (TextView) findViewById(R.id.resultTitle);

        int passMark = 0;


        // Getting the pass mark by obtaining the number of questions in the quiz and dividing by 2
        ParseQuery<ParseObject> query = new ParseQuery<>("Quiz");
        query.whereEqualTo("roomName", roomName);
        query.orderByDescending("createdAt");

        try {
            List<ParseObject> queryResult = query.find();
            if(queryResult.size() > 0){
                // Obtaining the list of questions of the quiz
                ArrayList<ArrayList<String>> questions = (ArrayList<ArrayList<String>>) queryResult.get(0).get("questions");

                // Obtaining the quiz id
                quizId = queryResult.get(0).getObjectId();

                // If the number of questions is even, than just divide the number of questions by 2, if its odd, add 1 and divide it by 2.
                if (questions.size() % 2 == 0) {
                    passMark =  questions.size() / 2;
                } else {
                    passMark =  (questions.size() +1) / 2;
                }

                // Displaying the title of the quiz
                resultTitle.setText("Results of quiz: " + queryResult.get(0).get("title").toString());

            } else{
                // If no quiz was found, redirecting the user to the home page
                Toast.makeText(ViewQuizResults.this,"No quiz Found!", Toast.LENGTH_LONG).show();
                finish();
                //Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
                //startActivity(intent);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Obtaining all the results the particular quiz
        ParseQuery<ParseObject> query2= new ParseQuery<>("QuizAnswers");
        query2.whereEqualTo("quizId",quizId);

        final int finalPassMark = passMark;

        try {
            List<ParseObject> queryResult2 = query2.find();
            if(queryResult2.size() > 0){
                // The no of Tries are the number of rows in the database with the particular quizId
                noOfTries.setText(String.valueOf(queryResult2.size()));

                float noOfPassingMarks = 0;

                // Going through all the marks and adding 1 to a variable if the mark is greater or equal to the passing mark
                for(ParseObject result : queryResult2){
                    if (Integer.parseInt(result.get("noOfCorrectAns").toString()) >= finalPassMark) {
                        System.out.println("HERE");
                        noOfPassingMarks++;
                    }
                }

                // The pass percentage is the number of passing marks over the total number of marks time 100
                passPercentage.setText(String.valueOf((noOfPassingMarks/queryResult2.size())*100));


            } else{
                // If not quiz results have been found
                noOfTries.setText("No quiz results!");
                passPercentage.setText("No quiz results!");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }



    }
}
