package com.parse.starter;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;



public class RoomList extends AppCompatActivity {

    public void generateRoomList(final ArrayList<String> rooms,  final ArrayAdapter arrayAdapter){
        rooms.clear();



        final ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Rooms");

        //Getting all rooms
        query.whereExists("objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        for(ParseObject room : objects){
                            rooms.add(room.getString("title"));
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void searchRoomTable(String s,final ArrayList<String> rooms, final ArrayAdapter arrayAdapter) throws ParseException {

        ArrayList<String> matchedRoomsListView = new ArrayList<>();

        ListView roomListView = (ListView) findViewById(R.id.roomsListView);


        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Rooms");

        query.whereContains("title",s);
        List<ParseObject> matchedRooms = query.find();

        if (matchedRooms.size() == 0) {
            rooms.clear();
            rooms.add("No Rooms found!");
            arrayAdapter.notifyDataSetChanged();
        }

        else {
            rooms.clear();
            for(ParseObject room : matchedRooms) {
                rooms.add(room.getString("title"));
            }
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        setTitle("Rooms");

        final ArrayList<String> rooms = new ArrayList<>();

        ArrayAdapter arrayAdapter;
        ListView roomsListView = (ListView) findViewById(R.id.roomsListView);

        roomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // CHANGE TO NEW ROOM CLASS
                Intent intent = new Intent(getApplicationContext(), Chat.class);
                // TO PUT EXTRA IS NOT i but the title or object id and search for it
                intent.putExtra("username", rooms.get(i));

                startActivity(intent);
            }
        });

        arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,rooms){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.plaintxt_border));

                return view;
            }
        };

        roomsListView.setAdapter(arrayAdapter);

        generateRoomList(rooms,arrayAdapter);


        // For search bar
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            SearchView search = (SearchView) findViewById(R.id.search);
            search.setQueryHint("Search for room...");

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return false;
                }
            });


            }
        }


    }
