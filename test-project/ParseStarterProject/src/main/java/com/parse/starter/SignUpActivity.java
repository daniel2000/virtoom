package com.parse.starter;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUpActivity extends AppCompatActivity implements View.OnKeyListener, View.OnClickListener {

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {

        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

            signUp(view);

        }

        return false;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.background || view.getId() == R.id.imageView) {

            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


        }

    }

    public void signUp(View view) {
        EditText nameText = (EditText) findViewById(R.id.name);
        EditText emailText = (EditText) findViewById(R.id.email);
        EditText passwordText = (EditText) findViewById(R.id.password);
        EditText institutionText = (EditText) findViewById(R.id.institution);

        String nameSurname = nameText.getText().toString();
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String institution = institutionText.getText().toString();

        if (nameSurname.matches("") || email.matches("") || password.matches("") || institution.matches("")){
            Toast.makeText(this,"Enter all fields!", Toast.LENGTH_LONG).show();
        } else {

            ParseUser user = new ParseUser();

            user.setUsername(nameSurname);
            user.setEmail(email);
            user.setPassword(password);
            user.put("institution", institution);


            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {

                        Toast.makeText(SignUpActivity.this, "Sign Up Successful", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(getApplicationContext(), UserListActivity.class);
                        startActivity(intent);

                    } else {

                        Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        EditText institution = (EditText) findViewById(R.id.institution);
        ImageView logo = (ImageView) findViewById(R.id.imageView);
        ConstraintLayout background = (ConstraintLayout) findViewById(R.id.background);


        institution.setOnKeyListener(this);
        background.setOnClickListener(this);
        logo.setOnClickListener(this);


    }
}
