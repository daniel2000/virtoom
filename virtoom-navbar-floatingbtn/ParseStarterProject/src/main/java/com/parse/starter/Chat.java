package com.parse.starter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Chat extends AppCompatActivity{

    String activeUser = "";
    String currentUser = "";

    List<Map<String,String>> messageData;
    SimpleAdapter simpleAdapter;

    //ArrayList<String> messages = new ArrayList<>();
    //ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setUpChat();
    }

    private void setUpChat(){
        Intent intent = getIntent();

        messageData = new ArrayList<>();

        activeUser = intent.getStringExtra("username");
        currentUser = ParseUser.getCurrentUser().getUsername();

        setTitle("Chatting with " + activeUser);

        final ListView chatListView = (ListView) findViewById(R.id.chatListView);

        simpleAdapter = new SimpleAdapter(Chat.this, messageData, android.R.layout.simple_list_item_2, new String[] {"content", "date"}, new int[] {android.R.id.text1, android.R.id.text2}){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView textView = (TextView) view.findViewById(android.R.id.text1);

                SpannableString ss = new SpannableString(textView.getText().toString());

                ForegroundColorSpan appliedColor;

                if(textView.getText().toString().split(":")[0].compareTo(currentUser) == 0){
                    appliedColor = new ForegroundColorSpan(Color.BLUE);
                }else{
                    appliedColor = new ForegroundColorSpan(Color.DKGRAY);
                }

                ss.setSpan(appliedColor, textView.getText().toString().indexOf(":")+1, textView.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                textView.setText(ss);

                return view;
            }
        };

        chatListView.setAdapter(simpleAdapter);

        //arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, messages);

        //chatListView.setAdapter(arrayAdapter);

        ParseQuery<ParseObject> query1 = new ParseQuery<>("Message");

        query1.whereEqualTo("sender", currentUser);
        query1.whereEqualTo("recipient", activeUser);

        ParseQuery<ParseObject> query2 = new ParseQuery<>("Message");

        query2.whereEqualTo("recipient", currentUser);
        query2.whereEqualTo("sender", activeUser);

        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(query1);
        queries.add(query2);

        ParseQuery<ParseObject> query = ParseQuery.or(queries);

        query.orderByAscending("createdAt");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        //messages.clear();
                        messageData.clear();
                        for(ParseObject message : objects){
                            Map<String,String> messageInfo = new HashMap<>();

                            messageInfo.put("content", message.getString("message"));

                            messageInfo.put("date", message.getString("actualDate"));

                            messageData.add(messageInfo);
                            //messages.add(message.getString("message"));
                        }
                        simpleAdapter.notifyDataSetChanged();
                        //arrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void sendChat(View view){
        final EditText chatEditText = (EditText) findViewById(R.id.chatEditText);

        final String messageContent = chatEditText.getText().toString();

        final Calendar cal = Calendar.getInstance();

        final SimpleDateFormat dateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        ParseObject message = new ParseObject("Message");

        message.put("sender", currentUser);
        message.put("recipient", activeUser);
        message.put("message", currentUser + ": " + messageContent);
        message.put("actualDate", dateTime.format(cal.getTime()));

        chatEditText.setText("");

        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    Map<String,String> messageInfo = new HashMap<>();

                    messageInfo.put("content",currentUser + ": " +  messageContent);

                    messageInfo.put("date", dateTime.format(cal.getTime()));

                    messageData.add(messageInfo);
                    simpleAdapter.notifyDataSetChanged();
                    //messages.add(currentUser + ": " +messageContent);
                    //arrayAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
