package com.parse.starter;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RoomListFragment extends Fragment {
    ArrayList<String> rooms = new ArrayList<>();
    ArrayList<String> topics = new ArrayList<>();
    ArrayList<String> admins = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_room_list, container, false);

        generateRoomList(v);

        FloatingActionButton floatingActionButton = (FloatingActionButton) v.findViewById(R.id.create_room);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = CreateRoomDialog.newInstance();
                ((CreateRoomDialog)dialog).setCallback(new CreateRoomDialog.Callback() {
                    @Override
                    public void onActionClick(String name) {
                        Toast.makeText(getActivity(),name,Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }
        });

        return v;
    }

    public void generateRoomList(View v){
        rooms.clear();

        ListView roomsListView = (ListView) v.findViewById(R.id.roomsList);

        roomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity().getApplicationContext(), RoomChat.class);
                intent.putExtra("roomName", rooms.get(i));
                intent.putExtra("topic", topics.get(i));
                intent.putExtra("admin",admins.get(i));

                startActivity(intent);
            }
        });

        arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(),android.R.layout.simple_list_item_1,rooms){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(ContextCompat.getColor(getActivity().getApplicationContext(),R.color.plaintxt_border));

                return view;
            }
        };
        roomsListView.setAdapter(arrayAdapter);

        ParseQuery<ParseObject> query = new ParseQuery<>("Room");


        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        for(ParseObject room : objects){
                            rooms.add(room.getString("title"));
                            topics.add(room.getString("topic"));
                            admins.add(room.getString("creator"));
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

}
