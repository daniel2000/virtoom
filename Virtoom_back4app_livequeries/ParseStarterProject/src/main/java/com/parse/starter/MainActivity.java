package com.parse.starter;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener{

    boolean signUpMode = true;

    public void redirect(){
        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        username.setText("");
        password.setText("");

        if(ParseUser.getCurrentUser() != null){
            Intent intent = new Intent(getApplicationContext(), FragmentLoader.class);
            startActivity(intent);
            finish();
        }
    }

    public void signupLogin(View view){

        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        if(signUpMode){
            ParseUser user = new ParseUser();
            user.setUsername(username.getText().toString());
            user.setPassword(password.getText().toString());

            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null){
                        redirect();
                    }else{
                        String message = e.getMessage();

                        if(message.toLowerCase().contains("java")){
                            message = e.getMessage().substring(e.getMessage().indexOf(" "));
                        }

                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            ParseUser.logInInBackground(username.getText().toString(), password.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if(e == null){
                        redirect();
                    }else{
                        String message = e.getMessage();

                        if(message.toLowerCase().contains("java")){
                            message = e.getMessage().substring(e.getMessage().indexOf(" "));
                        }

                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    public void changeMode(View view){
        Button btn = (Button) findViewById(R.id.signupLogin);

        TextView info = (TextView) findViewById(R.id.changeModeTextView);

        if(signUpMode){
            btn.setText("Login");
            info.setText("or, Signup");
            signUpMode = false;
        }else{
            btn.setText("Signup");
            info.setText("or, Login");
            signUpMode = true;
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {

        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            signupLogin(view);
        }

        return false;
    }

    @Override
    public void onClick(View view) {

         if (view.getId() == R.id.background || view.getId() == R.id.logo) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Virtoom");

        EditText password = (EditText) findViewById(R.id.password);
        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());

        redirect();

        setupListiners();

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

    private void setupListiners(){
        RelativeLayout background = (RelativeLayout) findViewById(R.id.background);
        background.setOnClickListener(this);

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setOnClickListener(this);
    }
}