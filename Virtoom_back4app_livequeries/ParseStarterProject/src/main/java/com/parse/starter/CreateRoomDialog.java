package com.parse.starter;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class CreateRoomDialog extends DialogFragment implements View.OnClickListener{

    private Callback callback;

    static CreateRoomDialog newInstance(){
        return new CreateRoomDialog();
    }

    public void setCallback(Callback callback){
        this.callback=callback;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL,R.style.createroomTheme);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final String admin = ParseUser.getCurrentUser().getUsername();

        final View view =inflater.inflate(R.layout.activity_create_room_dialog, container,false);

        RelativeLayout di_background = (RelativeLayout) view.findViewById(R.id.dialog_back);
        di_background.setOnClickListener(this);

        ImageButton close = (ImageButton) view.findViewById(R.id.close_dialog);
        Button create =(Button) view.findViewById(R.id.createRoom);

        close.setOnClickListener(this);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText title=(EditText) view.findViewById(R.id.title);
                final EditText topic=(EditText) view.findViewById(R.id.topic);
                final EditText duration=(EditText) view.findViewById(R.id.duration);


                ParseObject room = new ParseObject("Room");

                room.put("title",title.getText().toString());
                room.put("topic",topic.getText().toString());
                room.put("duration",duration.getText().toString());
                room.put("creator", admin);

                room.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null){
                            dismiss();
                            //Redirect user to the room instantly!
                            Intent intent = new Intent(getActivity().getApplicationContext(), RoomChat.class);
                            intent.putExtra("roomName", title.getText().toString());
                            intent.putExtra("topic", topic.getText().toString());
                            intent.putExtra("admin", admin);

                            startActivity(intent);
                        }else{
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.close_dialog){
            dismiss();
        }

        if(v.getId() == R.id.dialog_back){
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public interface Callback{
        void onActionClick(String name);
    }


}
