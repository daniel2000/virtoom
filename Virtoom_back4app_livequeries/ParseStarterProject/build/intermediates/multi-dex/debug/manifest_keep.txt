-keep class com.parse.starter.StarterApplication {
    <init>();
    void attachBaseContext(android.content.Context);
}
-keep class com.parse.starter.MainActivity { <init>(); }
-keep class com.parse.starter.UserList { <init>(); }
-keep class com.parse.starter.Chat { <init>(); }
-keep class com.parse.starter.FragmentLoader { <init>(); }
-keep class com.parse.starter.RoomList { <init>(); }
-keep class com.parse.starter.CreateRoomDialog { <init>(); }
-keep class com.parse.starter.RoomChat { <init>(); }
